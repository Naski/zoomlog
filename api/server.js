const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors');

const port = 5000;
//const port = 8080;
let users = require('./testusers.json');
let tiles = require('./testdata.json');

const app = express();
app.use(bodyParser.json());
app.use(cors())

app.get("/tiles", (req, res) => {
  let userGroups = getUserByUuid(req.query.uuid).groups;
  console.log(userGroups);
  let tiles = getTilesByGroups(userGroups);
  res.send(tiles);
});
  
app.get("/users/", (req, res) => {
  let user = getUserByUuid(req.query.uuid);
  if(user == 404) {
    res.status(404).send("Not found");
  } else {
    res.send(user);
  }
});
app.listen(port, () => {
  console.log("App's running on port "+port);
}); 
  function getTilesByGroups(groups) {
    let result = [];
      tiles.forEach(tile => {
        groups.forEach(group => {
          if(tile.groupId == group) {
            console.log("group matched!");
            result[result.length] = tile;
          }
        });
      });
    return result;
  } 
  function getUserByUuid(uuid) {
    let result = 404;
    users.forEach(user => {
      console.log(user.uuid +" == "+ uuid);
      if(user.uuid == uuid) {
        console.log("users match");
        result = user;
      }
    });
    return result;
  }